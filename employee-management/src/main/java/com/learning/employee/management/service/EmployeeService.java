package com.learning.employee.management.service;

import com.learning.employee.management.dao.EmployeeRepo;
import com.learning.employee.management.dto.Employee;
import com.learning.employee.management.entity.EmployeeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;

    public List<Employee> getAllEmployees() {
        return employeeRepo.findAll().stream().map(EmployeeEntity::transformToDto).toList();
    }

    public Employee getEmployee(long id) {
        EmployeeEntity employee = employeeRepo.findById(id).orElseThrow(
                () -> new RuntimeException(String.format(
                        "Employee with id %s not present", id)
                )
        );
        return employee.transformToDto();
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepo.save(employee.transformToEntity()).transformToDto();
    }

    public Employee updateEmployee(Employee employee) {
        long id = employee.getId();
        if (id == 0 || !employeeRepo.existsById(id)) {
            throw new RuntimeException(String.format("Employee with id %s not present", id));
        }
        return createEmployee(employee);
    }

    public void deleteEmployee(long id) {
        if (!employeeRepo.existsById(id)) {
            throw new RuntimeException(String.format("Employee with id %s not present", id));
        }
        employeeRepo.deleteById(id);
    }
}
