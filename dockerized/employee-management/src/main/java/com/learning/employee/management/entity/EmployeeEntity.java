package com.learning.employee.management.entity;

import com.learning.employee.management.dto.Employee;
import jakarta.persistence.*;

import java.time.Instant;

@Entity
@Table(name = "employees", schema = "employee_management")
public class EmployeeEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name", nullable = false, length = 25)
    private String name;
    @Column(name = "age", nullable = false)
    private int age;
    @Column(name = "doj", nullable = false, updatable = false)
    private Instant doj;
    @Column(name = "isActive", nullable = false)
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Instant getDoj() {
        return doj;
    }

    public void setDoj(Instant doj) {
        this.doj = doj;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", doj=" + doj +
                ", isActive=" + isActive +
                '}';
    }

    public Employee transformToDto() {
        Employee employee = new Employee();
        employee.setId(this.id);
        employee.setName(this.name);
        employee.setAge(this.age);
        employee.setDoj(this.doj);
        employee.setIsActive(this.isActive);
        return employee;
    }
}
