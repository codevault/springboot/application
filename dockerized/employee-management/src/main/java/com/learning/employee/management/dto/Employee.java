package com.learning.employee.management.dto;

import com.learning.employee.management.entity.EmployeeEntity;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;

import java.time.Instant;


public class Employee {

    private long id;
    @Size(min = 5, max = 50, message = "Name must be between 5 and 25 characters")
    private String name;
    @Min(value = 18, message = "Age must be more than 18")
    @Max(value = 60, message = "Age must be less than 60")
    private int age;
    @Future(message = "Date should be in the future")
    private Instant doj;
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Instant getDoj() {
        return doj;
    }

    public void setDoj(Instant doj) {
        this.doj = doj;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", doj=" + doj +
                ", isActive=" + isActive +
                '}';
    }

    public EmployeeEntity transformToEntity() {
        EmployeeEntity employee = new EmployeeEntity();
        employee.setId(this.id);
        employee.setName(this.name);
        employee.setAge(this.age);
        employee.setDoj(this.doj);
        employee.setIsActive(this.isActive);
        return employee;
    }
}
